class Constants
  IMAGE_EXTENSIONS = ["png", "apng", "avif", "bmp", "ico", "tif", "tiff", "jpg", "jpeg", "gif", "svg", "webp"]
  VIDEO_EXTENSIONS = ["mp4", "m4v", "ogv", "mov", "webm", "mkv"]
  AUDIO_EXTENSIONS = ["mp3", "aac", "wav", "flac", "ogg"]
end

