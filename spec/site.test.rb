require 'date'
require_relative './spec_helper'
require_relative '../site.rb'

class FakeFileLoader
  def initialize()
    post_1 = <<~EOS
      # Example Title

      Some post content.

      * A list
      * Another thing

      ## Second Header

      ### Third header
      EOS
    post_2 = <<~EOS
      Something else
      Another p
      yeah yeah yeah
      EOS
    frame_html = "Before text\n{{ content }}\nAfter text"
    frame_xml = "Before XML\n{{ content }}\nAfter XML"

    @files = {
      "./input/.frame.html" => frame_html,
      "./input/.frame.xml" => frame_xml,
      "./input/2022-02-02-post-1.gmi" => post_1,
      "./input/2001-12-25-post-2.gmi" => post_2,
    }

    @read_files = []
    @written_files = {}
  end

  def read(filename)
    @read_files << filename
    return @files[filename]
  end

  def write(filename, content)
    @written_files[filename] = content
  end

  def split(filename)
    return File.split(filename)
  end

  def join(*words)
    return File.join(*words)
  end

  def set_file(filename, content)
    @files[filename] = content
  end

  def get_read_files()
    return @read_files
  end

  def get_written_files()
    return @written_files
  end

  def get_file_names()
    return @files.keys
  end

  def get_post_names()
    @files.keys.select { |filename| filename.end_with?('.gmi') }
  end
end

class FakeParser
  def initialize
    @seen = []
  end

  def parse(text)
    @seen << text
    return text
  end

  def get_seen_text
    return @seen
  end
end

RSpec.describe SiteBuilder do
  @input = nil
  @output = nil
  @sitebuilder = nil
  @file_mock = nil
  @parser = nil

  before(:each) do
    @input = './input'
    @output = '.'
    @site_url = 'http://www.example.com'
    @file_mock = FakeFileLoader.new()
    @parser = FakeParser.new()
    @sitebuilder = SiteBuilder.new(@input, @output, @site_url, @file_mock, @parser)
  end

  describe "get_frame_halves" do
    content_delimiter = '{{ content }}'
    frame_filename = '.frame.html'

    it "should load the file from the filepath" do
      filepath = File.join(@input, frame_filename)
      @sitebuilder.get_frame_halves(frame_filename)
      files = @file_mock.get_read_files()
      expect(files).to include(filepath)
    end

    it "should throw a StandardError if no delimiter was found in the frame" do
      text_before = "text before"
      text_after = "text after"
      frame_html = text_before + text_after
      filepath = File.join(@input, frame_filename)
      @file_mock.set_file(filepath, frame_html)
      expect { @sitebuilder.get_frame_halves(frame_filename) } .to raise_error(StandardError)
    end

    it "should get the text before and after the delimiter" do
      text_before = "text before"
      text_after = "text after"
      frame_html = text_before + content_delimiter + text_after
      filepath = File.join(@input, frame_filename)
      @file_mock.set_file(filepath, frame_html)
      before, after = @sitebuilder.get_frame_halves(frame_filename)
      expect(before).to match(text_before)
      expect(after).to match(text_after)
    end

    it "should not strip the resulting text of whitespace" do
      text_before = "text before  "
      text_after = "  text after"
      frame_html = text_before + content_delimiter + text_after
      filepath = File.join(@input, frame_filename)
      @file_mock.set_file(filepath, frame_html)
      before, after = @sitebuilder.get_frame_halves(frame_filename)
      expect(before).to match(text_before)
      expect(after).to match(text_after)
    end
  end

  describe "get_title" do
    title = "First Header"
    filename = "default.gmi"
    post = nil

    before(:each) do
      post = <<~EOS
        # #{title}

        Some post content.

        * A list
        * Another thing

        ## Second Header

        ### Third header
        EOS
    end

    it "should return filename without extension if no header lines are present" do
      post = "Not a header"
      filename_no_ext = filename.sub(/\.gmi/, '')
      parsed_title = @sitebuilder.get_title(post, filename)
      expect(parsed_title).to eq(filename_no_ext)
    end

    it "should get text content of first header line in file" do
      parsed_title = @sitebuilder.get_title(post, filename)
      expect(parsed_title).to match(title)
    end

    it "should get text content of header line even if it's not the first line" do
      post = <<~EOS
        A starting line!

        # #{title}
        EOS
      parsed_title = @sitebuilder.get_title(post, filename)
      expect(parsed_title).to match(title)
    end
  end

  describe "get_date" do
    year, month, day = 2022, 2, 2
    filename = "%04d-%02d-%02d-a-title.gmi" % [year, month, day]

    it "should return a DateTime object" do
      dt = @sitebuilder.get_date(filename)
      expect(dt).to be_a_kind_of(DateTime)
    end

    it "should convert filename date to correct date" do
      expected_dt = DateTime.new(year, month, day)
      dt = @sitebuilder.get_date(filename)
      expect(dt).to eq(expected_dt)
    end
  end

  describe "get_pages_data" do
    page_filenames = nil

    before(:each) do
      page_filenames = @file_mock.get_post_names()
    end

    it "should return an array" do
      pages_data = @sitebuilder.get_pages_data(page_filenames)
      expect(pages_data).to be_a_kind_of(Array)
    end

    it "should return an entry in array for each page" do
      pages_data = @sitebuilder.get_pages_data(page_filenames)
      expect(pages_data.length).to eq(page_filenames.length)
    end

    it "should have certain properties in each page entry of hash" do
      pages_data = @sitebuilder.get_pages_data(page_filenames)
      pages_data.each do |data|
        expect(data.keys).to match_array([:content, :date, :filepath, :title])
        expect(data[:content]).to be_a_kind_of(String)
        expect(data[:date]).to be_a_kind_of(DateTime)
        expect(data[:filepath]).to be_a_kind_of(String)
        expect(data[:title]).to be_a_kind_of(String)
      end
    end

    it "should ensure the file contents always end with a newline" do
      file_without_newline_at_end = "# Test\n## Only Headers"
      base_filename = '2000-01-01-no-newline.gmi'
      new_filepath = File.join(@input, base_filename)
      @file_mock.set_file(new_filepath, file_without_newline_at_end)
      updated_page_filenames = @file_mock.get_post_names()
      pages_data = @sitebuilder.get_pages_data(updated_page_filenames)
      pages_data.each { |data| expect(data[:content]).to end_with("\n") }
    end

    it "should set \"filepath\" to base filename" do
      pages_data = @sitebuilder.get_pages_data(page_filenames)
      pages_data.each do |data|
        folder, base_filename = File.split(data[:filepath])
        expect(folder).to eq('.')
        expect(base_filename).not_to include('/')
      end
    end

    it "should set \"date\" to nil if \"dated\" option is set to false on invocation" do
      pages_data = @sitebuilder.get_pages_data(page_filenames, dated = false)
      pages_data.each { |data| expect(data[:date]).to be_nil() }
    end

    it "should sort return array of pages data by date" do
      pages_data = @sitebuilder.get_pages_data(page_filenames)
      pages_data.each_with_index do |data, index|
        # Skip first, as nothing to compare
        if index == 0
          next
        end
        current_page_date = data[:date]
        previous_page_date = pages_data[index - 1][:date]
        expect(current_page_date < previous_page_date).to be_truthy()
      end
    end
  end

  describe "fill_frame_and_write_html" do
    before = nil
    after = nil
    html_filepath = nil
    data = nil

    before(:each) do
      before, after = ["Before text", "After text"]
      data = {
        content: "# Heading\nSome paragraph\nYeah",
        html: "<h1>Heading</h1><p>Some paragraph</p><p>Yeah</p>",
        date: DateTime.new(2000, 1, 1),
        filepath: "filename.gmi",
        title: "The Title",
      }
      html_filepath = File.join(@output, data[:filepath].sub(/\.gmi/, '.html'))
    end

    it "should write html file to output folder, replacing the \"gmi\" extension" do
      @sitebuilder.fill_frame_and_write_html(data, before, after)
      written_files = @file_mock.get_written_files
      expect(written_files).to have_key(html_filepath)
      expect(written_files[html_filepath]).to be_a_kind_of(String)
    end

    it "should insert parsed content between \"before\" and \"after\" sections" do
      @sitebuilder.fill_frame_and_write_html(data, before, after)
      written_files = @file_mock.get_written_files()
      expect(written_files[html_filepath]).to start_with(before)
      expect(written_files[html_filepath]).to include(data[:html])
      expect(written_files[html_filepath]).to end_with(after)
    end

    it "should replace \"{{ title }}\" with title if present" do
      before_with_title = "Before text\n{{ title }}\n"
      before_with_title_filled = before_with_title.sub(/\{\{ title \}\}/, data[:title])
      @sitebuilder.fill_frame_and_write_html(data, before_with_title, after)
      written_files = @file_mock.get_written_files()
      expect(written_files[html_filepath]).not_to start_with(before_with_title)
      expect(written_files[html_filepath]).to start_with(before_with_title_filled)
    end
  end

  describe "populate_index" do
    posts_data = nil

    before(:each) do
      posts_data = [
        {
          title: "Filename 1 Title",
          filepath: "filename-1.gmi",
          date: DateTime.new(2019, 2, 2),
          content: "Test",
        },
        {
          title: "Filename 2 Title",
          filepath: "filename-2.gmi",
          date: DateTime.new(2020, 1, 1),
          content: "Test",
        },
        {
          title: "Filename 3 Title",
          filepath: "filename-3.gmi",
          date: DateTime.new(2018, 6, 6),
          content: "Test",
        },
      ]
    end

    it "should return a string" do
      index = @sitebuilder.populate_index(posts_data)
      expect(index).to be_a_kind_of(String)
    end

    it "should return a series of gemtext-formatted links" do
      index = @sitebuilder.populate_index(posts_data)
      links = index.split("\n")
      expect(links.size).not_to eq(0)
      links.each { |link| expect(link).to start_with("=> ") }
    end

    it "should contain place \"filename.html\" as href and titles as text for each link" do
      index = @sitebuilder.populate_index(posts_data)
      links = index.split("\n")
      expect(links.size).not_to eq(0)
      posts_data.each do |data|
        _, base_filename = File.split(data[:filepath])
        html_filename = base_filename.sub(/\.gmi/, ".html")
        link = links.detect { |link| link.match(html_filename) }
        expect(link).not_to be_nil()
        _, href, title = link.split(' ', 3)
        expect(href).to eq(html_filename)
        date_text = data[:date].strftime('%Y-%m-%d')
        expect(title).to eq("#{date_text} - #{data[:title]}")
      end
    end
  end

  describe "create_rss" do
    content_delimiter = '{{ content }}'
    frame_filename = nil
    rss_feed_filepath = nil
    posts_data = nil

    before(:each) do
      frame_filename = File.join(@input, '.frame.xml')
      rss_feed_filepath = File.join(@output, 'rss.xml')
      posts_data = [
        {
          content: "Test",
          date: DateTime.new(2019, 2, 2),
          filepath: "filename-1.gmi",
          html: "<p>Test</p>",
          title: "Filename 1 Title",
        },
        {
          content: "Test",
          date: DateTime.new(2020, 1, 1),
          filepath: "filename-2.gmi",
          html: "<p>Test</p>",
          title: "Filename 2 Title",
        },
        {
          content: "Test",
          date: DateTime.new(2018, 6, 6),
          filepath: "filename-3.gmi",
          html: "<p>Test</p>",
          title: "Filename 3 Title",
        },
      ]
    end

    it "should write to \"rss.xml\"" do
      @sitebuilder.create_rss(posts_data)
      written_files = @file_mock.get_written_files
      expect(written_files).to include(rss_feed_filepath)
    end

    it "should include \".frame.xml\" data that surrounds the content delimiter in the XML file" do
      text_before = "Different text before"
      text_after = "Different text after"
      frame_xml = text_before + content_delimiter + text_after
      @file_mock.set_file(frame_filename, frame_xml)
      @sitebuilder.create_rss(posts_data)
      rss_feed = @file_mock.get_written_files()[rss_feed_filepath]
      expect(rss_feed).to start_with(text_before)
      expect(rss_feed).to end_with(text_after)
    end

    it "should include an <item> for each post in posts_data" do
      @sitebuilder.create_rss(posts_data)
      rss_feed = @file_mock.get_written_files()[rss_feed_filepath]
      items = rss_feed.scan(/<item>.+?<\/item>/m)
      expect(items.size).to eq(posts_data.size)
    end

    it "should include a maximum of 10 <item> elements" do
      # This is 30 posts
      lots_of_posts_data = posts_data * 10
      @sitebuilder.create_rss(lots_of_posts_data)
      rss_feed = @file_mock.get_written_files()[rss_feed_filepath]
      items = rss_feed.scan(/<item>.+?<\/item>/m)
      expect(items.size <= 10).to be_truthy()
    end

    it "should populate each <entry> with specific elements" do
      @sitebuilder.create_rss(posts_data)
      rss_feed = @file_mock.get_written_files()[rss_feed_filepath]
      items = rss_feed.scan(/<item>.+?<\/item>/)
      items.each do |item|
        expect(item).to match(/<title>.+?<\/title>/)
        expect(item).to match(/<link>.+?<\/link>/)
        expect(item).to match(/<guid>.+?<\/guid>/)
        expect(item).to match(/<pubDate>.+?<\/pubDate>/)
        expect(item).to match(/<description>.+?<\/content>/)
      end
    end
  end
end

