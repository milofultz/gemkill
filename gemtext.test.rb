require_relative './spec_helper'
require_relative '../gemtext'
require_relative '../constants'

RSpec.describe GemText do
  before(:each) do
    @parser = GemText.new
  end

  it "handles strings as input" do
    text = "paragraph\nanother paragraph"
    parsed_text = @parser.parse(text)
    expect(parsed_text).to be_a_kind_of(String)
  end

  it "handles arrays as input" do
    text = [
      "paragraph",
      "another paragraph"
    ]
    parsed_text = @parser.parse(text)
    expect(parsed_text).to be_a_kind_of(String)
  end

  it "parses nothing as nothing" do
    text = [""]
    parsed_text = @parser.parse(text)
    expect(parsed_text).to eq("")
  end

  it "parses basic text" do
    text = ["A paragraph of text is on one line."]
    parsed_text = @parser.parse(text)
    expect(parsed_text).to eq("<p>#{text[0]}</p>")
  end

  it "parses multiple lines of basic text without space" do
    text = [
      "One paragraph",
      "Another paragraph",
      "Last paragraph",
    ]
    parsed_text = @parser.parse(text)
    expect(parsed_text).to eq("<p>#{text[0]}</p><p>#{text[1]}</p><p>#{text[2]}</p>")
  end

  it "parses multiple lines of different types without space" do
    text = [
      "One paragraph",
      "=> http://example.com",
      "Another paragraph",
    ]
    href = text[1][3..]
    parsed_text = @parser.parse(text)
    expect(parsed_text).to eq("<p>#{text[0]}</p><p><a href=\"#{href}\">#{href}</a></p><p>#{text[2]}</p>")
  end

  it "parses multiple lines with single blank lines between" do
    text = [
      "One paragraph",
      "",
      "Another paragraph",
      "",
      "Another paragraph",
    ]
    parsed_text = @parser.parse(text)
    expect(parsed_text).to eq("<p>#{text[0]}</p><p>#{text[2]}</p><p>#{text[4]}</p>")
  end

  it "parses multiple lines with multiple blank lines between" do
    text = [
      "One paragraph",
      "",
      "Another paragraph",
      "",
      "",
      "Another paragraph with a br/",
    ]
    parsed_text = @parser.parse(text)
    expect(parsed_text).to eq("<p>#{text[0]}</p><p>#{text[2]}</p><br/><p>#{text[5]}</p>")
  end

  # HEADERS

  it "parses headers up to three deep" do
    header_text = "Header Text"
    text = [
      "# #{header_text}",
      "## #{header_text}",
      "### #{header_text}",
    ]

    header_depth = 0
    for line in text
      header_depth += 1
      parsed_header = @parser.make_header(line)
      expect(parsed_header).to eq("<h#{header_depth}>#{header_text}</h#{header_depth}>")
    end
  end

  it "renders headers more than three deep as three deep" do
    header_text = "Header Text"
    text = [
      "#### #{header_text}",
      "##### #{header_text}",
      "###### #{header_text}",
    ]

    header_depth = 3
    for line in text
      header_depth += 1
      parsed_header = @parser.make_header(line)
      expect(parsed_header).not_to eq("<h#{header_depth}>#{header_text}</h#{header_depth}>")
      expect(parsed_header).to eq("<h3>#{header_text}</h3>")
    end
  end

  # LINKS

  it "parses links" do
    text = "=> http://www.example.com"
    parsed_text = @parser.make_link(text)
    url = text[3..]
    expect(parsed_text).to eq("<p><a href=\"#{url}\">#{url}</a></p>")
  end

  it "parses links with text" do
    text = "=> http://www.example.com Example text"
    parsed_text = @parser.make_link(text)
    text = text[3..]
    split_index = text.index(' ')
    url, link_text = text[...split_index], text[split_index + 1..]
    expect(parsed_text).to eq("<p><a href=\"#{url}\">#{link_text}</a></p>")
  end

  it "parses images" do
    for extension in Constants::IMAGE_EXTENSIONS
      text = "=> example-image.#{extension}"
      parsed_text = @parser.make_link(text)
      url = text[3..]
      expect(parsed_text).to eq("<figure><img src=\"#{url}\" /></figure>")
    end
  end

  it "parses images with alt text" do
    for extension in Constants::IMAGE_EXTENSIONS
      text = "=> example-image.#{extension} This is example alt text"
      parsed_text = @parser.make_link(text)
      text = text[3..]
      split_index = text.index(' ')
      url, alt_text = text[...split_index], text[split_index + 1..]
      expect(parsed_text).to eq("<figure><img src=\"#{url}\" alt=\"#{alt_text}\" /></figure>")
    end
  end

  it "parses audio" do
    for extension in Constants::AUDIO_EXTENSIONS
      text = "=> example-audio.#{extension}"
      parsed_text = @parser.make_link(text)
      url = text[3..]
      expect(parsed_text).to eq("<figure><audio controls src=\"#{url}\"><a href=\"#{url}\">#{url}</a></audio></figure>")
    end
  end

  it "parses audio with a caption" do
    for extension in Constants::AUDIO_EXTENSIONS
      text = "=> example-audio.#{extension} Audio caption"
      parsed_text = @parser.make_link(text)
      text = text[3..]
      split_index = text.index(' ')
      url, caption = text[...split_index], text[split_index + 1..]
      expect(parsed_text).to eq("<figure><figcaption>#{caption}</figcaption><audio controls src=\"#{url}\"><a href=\"#{url}\">#{caption}</a></audio></figure>")
    end
  end

  it "parses video" do
    for extension in Constants::VIDEO_EXTENSIONS
      text = "=> example-video.#{extension}"
      parsed_text = @parser.make_link(text)
      url = text[3..]
      expect(parsed_text).to eq("<figure><video controls src=\"#{url}\" type=\"video/#{extension}\"><a href=\"#{url}\">#{url}</a></video></figure>")
    end
  end

  it "parses video with a caption" do
    for extension in Constants::VIDEO_EXTENSIONS
      text = "=> example-audio.#{extension} Video caption"
      parsed_text = @parser.make_link(text)
      text = text[3..]
      split_index = text.index(' ')
      url, caption = text[...split_index], text[split_index + 1..]
      expect(parsed_text).to eq("<figure><figcaption>#{caption}</figcaption><video controls src=\"#{url}\" type=\"video/#{extension}\"><a href=\"#{url}\">#{caption}</a></video></figure>")
    end
  end

  # LISTS

  it "parse list items" do
    line = "* this is a list item"
    parsed_text = @parser.make_list_item(line)
    expect(parsed_text).to eq("<li>#{line[2..]}</li>")
  end

  it "parses lists with a single item" do
    text = "* this is a list item"
    parsed_text = @parser.parse(text)
    expect(parsed_text).to eq("<ul><li>#{text[2..]}</li></ul>")
  end

  it "parses lists with multiple items" do
    text = [
      "* this is a list item",
      "* this is too",
    ]
    parsed_text = @parser.parse(text)
    list_text = text.map { |item| item[2..] }
    expect(parsed_text).to eq("<ul><li>#{list_text[0]}</li><li>#{list_text[1]}</li></ul>")
  end

  # BLOCKQUOTES

  it "parses single line blockquotes into paragraphs" do
    text = "A blockquote."
    line = "> #{text}"
    parsed_text = @parser.make_blockquote_paragraph(line)
    expect(parsed_text).to eq("<p>#{text}</p>")
  end

  it "parses multiline blockquotes into individual paragraphs" do
    lines = [
      "> First line. It has lots of text.",
      "> Second line",
      "> Third one!"
    ]
    parsed_text = @parser.parse(lines)
    expect(parsed_text).to eq("<blockquote>"\
                                "<p>#{lines[0][2..]}</p>"\
                                "<p>#{lines[1][2..]}</p>"\
                                "<p>#{lines[2][2..]}</p>"\
                              "</blockquote>")
  end

  # CODE BLOCKS

  it "parses code blocks" do
    text = [
      "```",
      "some code",
      "ruby ruby ruby",
      "```",
    ]
    parsed_text = @parser.parse(text)
    expect(parsed_text).to eq("<pre><code>#{text[1]}\n#{text[2]}\n</code></pre>")
  end

  it "does not parse lines inside code blocks" do
    text = [
      "```",
      "> not a blockquote",
      "* not a list item",
      "```",
    ]
    parsed_text = @parser.parse(text)
    expect(parsed_text).to eq("<pre><code>#{text[1]}\n#{text[2]}\n</code></pre>")
  end

  it "does not strip whitespace in code blocks" do
    text = [
      "```",
      "  leading spaces",
      "trailing spaces  ",
      "    both    ",
      "```",
    ]
    parsed_text = @parser.parse(text)
    expect(parsed_text).to eq("<pre><code>#{text[1]}\n#{text[2]}\n#{text[3]}\n</code></pre>")
  end
end

