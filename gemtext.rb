require_relative 'constants'

class GemText
  def initialize()
    @header     = "#"
    @list       = "* "
    @blockquote = ">"
    @link       = "=>"
    @code       = "```"

    @all_line_types = [@header, @list, @blockquote, @link, @code]

    @line_number = 0
    @line = ""
    @at_end = false
  end

  def parse_file(filename)
    gemtext = File.read(filename).split("\n")
    parse(gemtext)
  end

  def make_header(line)
    depth = 0
    while line[depth] == @header
      depth += 1
    end
    text = line[depth..]
    depth = [depth, 3].min()
    text = text.strip()
    return "<h#{depth}>#{text}</h#{depth}>"
  end

  def make_list_item(line)
    text = line[@list.length..].strip()
    return "<li>#{text}</li>"
  end

  def make_blockquote_paragraph(line)
    text = line[@blockquote.length..].strip()
    return "<p>#{text}</p>"
  end

  def make_link(line)
    link_raw = line[@link.length..].strip()
    url, text = link_raw.split(' ', 2)
    extension = url.rpartition('.')[-1]

    output = ""

    if not url.nil?
      url = url.strip()
      if not text
        text = url
      else
        text = text.strip()
      end

      case extension
      when *Constants::IMAGE_EXTENSIONS
        output += "<figure><img src=\"#{url}\" "
        if text != url
          output += "alt=\"#{text}\" "
        end
        output += "/></figure>"
      when *Constants::AUDIO_EXTENSIONS
        output += "<figure>"
        if text != url
          output += "<figcaption>#{text}</figcaption>"\
                    "<audio controls src=\"#{url}\">"\
                      "<a href=\"#{url}\">#{text}</a>"
        else
          output += "<audio controls src=\"#{url}\">"\
                      "<a href=\"#{url}\">#{url}</a>"
        end
        output += "</audio></figure>"
      when *Constants::VIDEO_EXTENSIONS
        output += "<figure>"
        if text != url
          output += "<figcaption>#{text}</figcaption>"\
                    "<video controls src=\"#{url}\" type=\"video/#{extension}\">"\
                      "<a href=\"#{url}\">#{text}</a>"
        else
          output += "<video controls src=\"#{url}\" type=\"video/#{extension}\">"\
                      "<a href=\"#{url}\">#{url}</a>"
        end
        output += "</video></figure>"
      else
        output += "<p><a href=\"#{url}\">#{text}</a></p>"
      end
    end
  end

  def next_line_in(lines)
    @line_number += 1
    if @line_number >= lines.length
      @at_end = true
    else
      @line = lines[@line_number]
    end
  end

  def parse(lines)
    if lines.class == String
      lines = lines.split("\n")
    elsif lines.class != Array
      raise TypeError.new('An array of strings is required')
      return
    end

    body = ""

    @line_number = 0
    @line = lines[0]
    @at_end = @line_number > lines.length

    while not @at_end
      if @line.start_with?(/#{@header}{1,} /)
        body += make_header(@line)
        next_line_in(lines)
      elsif @line.start_with?(@list)
        body += '<ul>'
        while @line.start_with?(@list) and not @at_end
          body += make_list_item(@line)
          next_line_in(lines)
        end
        body += '</ul>'
      elsif @line.start_with?(@blockquote)
        body += '<blockquote>'
        while @line.start_with?(@blockquote) and not @at_end
          body += make_blockquote_paragraph(@line)
          next_line_in(lines)
        end
        body += '</blockquote>'
      elsif @line.start_with?(@link)
        body += make_link(@line)
        next_line_in(lines)
      elsif @line.start_with?(@code)
        body += "<pre><code>"
        next_line_in(lines)
        while not @line.start_with?(@code) and not @at_end
          body += @line + "\n"
          next_line_in(lines)
        end
        body += '</code></pre>'
        next_line_in(lines)
      elsif @line.strip() == ''
        next_line_in(lines)
        while @line.strip() == '' and not @at_end
          body += '<br/>'
          next_line_in(lines)
        end
      else
        body += "<p>" + @line.strip() + "</p>"
        next_line_in(lines)
      end
    end

    return body
  end
end

if __FILE__ == $0
  if ARGV[0]
    parser = GemText.new
    html = parser.parse_file(ARGV[0])
    puts(html)
  end
end

