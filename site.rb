require 'date'
require 'fileutils'
require_relative './gemtext.rb'

class SiteBuilder
  def initialize(
      input,
      output,
      site_url = nil,
      file = File,
      parser = GemText.new()
  )
    @input = input
    @output = output
    @site_url = site_url
    @file = file
    @parser = parser

    # Constants
    @CONTENT_DELIMITER = '{{ content }}'
    @GMI_EXTENSION_PATTERN = /\.gmi$/
    @HTML_FRAME_FILENAME = '.frame.html'
    @POST_PREFIX = /(\d{4})-(\d{2})-(\d{2})/
    @RESERVED_FILENAMES = [@HTML_FRAME_FILENAME, @XML_FRAME_FILENAME]
    @SITE_URL_DELIMITER = '{{ site_url }}'
    @TITLE_DELIMITER = '{{ title }}'
    @XML_FRAME_FILENAME = '.frame.xml'
  end

  def build()
    if not @file.directory?(@output)
      Dir.mkdir(@output)
    end

    before, after = self.get_frame_halves(@HTML_FRAME_FILENAME)
    post_files = []
    nonpost_files = []
    Dir.entries(@input).each do |f|
      filepath = File.join(@input, f)
      if f.start_with?('.')
        next
      elsif f.end_with?('gmi')
        if f.start_with?(@POST_PREFIX)
          post_files << filepath
        else
          nonpost_files << filepath
        end
      elsif not @RESERVED_FILENAMES.include?(f)
        destination = File.join(@output, f)
        FileUtils.copy(filepath, destination)
      end
    end
    posts_data = self.get_pages_data(post_files)
    posts_data.each do |data|
      data[:html] = convert_gmi_to_html(data[:content])
      self.fill_frame_and_write_html(data, before, after)
    end
    nonposts_data = self.get_pages_data(nonpost_files, dated = false)
    nonposts_data.each do |data|
      data[:html] = convert_gmi_to_html(data[:content])
      self.fill_frame_and_write_html(data, before, after)
    end
    index_of_posts = self.populate_index(posts_data)
    index_filepath = @file.join(@input, 'index.gmi')
    index_data = get_pages_data(index_filepath, dated = false)
    index_data = index_data[0]
    index_data[:content] += index_of_posts
    index_data[:html] = convert_gmi_to_html(index_data[:content])
    self.fill_frame_and_write_html(index_data, before, after)
    if not @site_url.nil?
      self.create_rss(posts_data)
    end
    puts("Build complete.")
  end

  def get_frame_halves(frame_filename)
    filepath = @file.join(@input, frame_filename)
    frame = @file.read(filepath)
    if frame.scan(@CONTENT_DELIMITER).empty?
      raise StandardError.new("Content delimiter not found in frame!\n\nDelimiter: #{@CONTENT_DELIMITER}\nFrame: #{frame}")
    end
    return frame.split(@CONTENT_DELIMITER, 2)
  end

  def get_title(post, filename)
    lines = post.split("\n")
    title = lines.detect { |line| line.start_with?("#") }
    if title
      # Remove leading hash and leading/trailing spaces
      return title.gsub!(/^#+\s?/, '').strip()
    else
      return filename.sub(@GMI_EXTENSION_PATTERN, '')
    end
  end

  def get_date(filename)
    string_date = filename.match(@POST_PREFIX)
    year, month, day = string_date[1].to_i(), string_date[2].to_i(), string_date[3].to_i()
    return DateTime.new(year, month, day)
  end

  def convert_gmi_to_html(gmi)
    return @parser.parse(gmi)
  end

  def get_pages_data(pages, dated = true)
    if pages.is_a?(String)
      pages = [pages]
    end
    pages_data = []

    # Render all the pages
    pages.each do |filepath|
      _, base_filename = @file.split(filepath)
      content = @file.read(filepath)
      if not content.end_with?("\n")
        content += "\n"
      end
      page_data = {
        content: content,
        date: dated ? self.get_date(base_filename) : nil,
        filepath: base_filename,
        title: self.get_title(content, base_filename),
      }
      pages_data << page_data
    end

    if dated
      pages_data = pages_data.sort { |a, b| b[:date] <=> a[:date] }
    end

    return pages_data
  end

  def fill_frame_and_write_html(data, before, after)
    content = data[:content]
    filepath = data[:filepath]
    title = data[:title]
    rendered_post_in_frame = before + data[:html] + after
    # Add unique title where user requested if requested
    rendered_post_in_frame = rendered_post_in_frame.gsub(@TITLE_DELIMITER, title)
    gmi_filename = filepath.sub(@GMI_EXTENSION_PATTERN, '.html')
    new_filepath = @file.join(@output, gmi_filename)
    @file.write(new_filepath, rendered_post_in_frame)
  end

  def populate_index(posts_data)
    post_links = []

    posts_data.each do |data|
      href = data[:filepath].sub(@GMI_EXTENSION_PATTERN, '.html')
      date_text = data[:date].strftime('%Y-%m-%d')
      post_links << "=> #{href} #{date_text} - #{data[:title]}"
    end

    return post_links.join("\n")
  end

  def create_rss(posts_data)
    before, after = self.get_frame_halves(@XML_FRAME_FILENAME)

    posts = ""
    posts_data[...10].each do |data|
      url = @site_url + '/' + data[:filepath].sub(@GMI_EXTENSION_PATTERN, '.html')
      content = data[:html].gsub('&', '&amp;').gsub('<', '&lt;').gsub('>', '&gt;')
      date = data[:date].rfc822()
      post_xml = <<~EOS
        <item>
          <title>#{data[:title]}</title>
          <link>#{url}</link>
          <guid>#{url}</guid>
          <description>#{content}</description>
          <pubDate>#{date}</pubDate>
        </item>
        EOS
      posts << post_xml
    end

    rss_file_contents = before + posts + after
    rss_file_contents = rss_file_contents.gsub(@SITE_URL_DELIMITER, @site_url)

    rss_feed_filepath = @file.join(@output, 'rss.xml')
    @file.write(rss_feed_filepath, rss_file_contents)
  end
end
