# Gemkill

A basic http static site generator using gemtext.

## Getting Started

You should have an input and an output directory. In your input folder you should have:

* An HTML frame file
* An optional [RSS 2.0](https://www.rssboard.org/rss-specification) frame file
* Posts/pages
* Any other files (CSS, etc.)

Then run `ruby gemkill.rb input output [site_url]`, with `input` and `output` being replaced by your input and output folders. A static HTML site will be build in the output folder. The `site_url` is optional, and if used, will also try to generate an Atom RSS feed.

### Frames

Your HTML frame file must be named `.frame.html` and live within your input folder. In this file, there must exist the substring `{{ content }}` which will be replaced by the page's content. There may optionally exist a substring `{{ title }}` which will be replaced by the title of the page. For example:

```html
<html>
  <head>
    <meta charset="UTF-8">
    <meta content='width=device-width, initial-scale=1' name='viewport'/>
    <title>{{ title }}</title>
    <link rel="stylesheet" href="style.css">
    <link rel="alternate" type="application/rss+xml" title="{{ title }}" href="rss.xml">
  </head>
  <body>
    {{ content }}
  </body>
</html>
```

The optional RSS frame file must be named `.frame.xml` and live within your input folder. In this file there must exist the substring `{{ content }}` where the post entries will go. You may also add the `{{ site_url }}` substring to be used in the links. For example:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0">
  <channel>
    <title>Site Title</title>
    <link>{{ site_url }}/rss.xml</link>
    <description>Site Description</description>
    {{ content }}
  </channel>
</rss>
```

### Posts/Pages

All posts and pages retain their same filenames after the build process, but with an `html` extension instead of `gmi`. The `{{ title }}` used in the frame will be pulled from the first header found in your post or page.

Posts will be populated on the index page. They must be formatted using [the standard gemtext spec](https://gemini.circumlunar.space/docs/gemtext.gmi). The only things that I have altered from the original spec is allowing images, video, and audio to be rendered as HTML elements. You can see more details by looking at the `constants` file for the formats used and the `make_link` function in the `gemtext` module for exact implementation.

The files must be named using the following format:

```
2007-06-15-a-post-title.gmi
^          ^            ^
A date formatted YYYY-MM-DD for the index page

           The title (not used)

                        Must be of extension `gmi`
```

All other pages must have a `gmi` extension and will not end up on the index page.

### Other Files

Any files that are not preceded by a `.` in their filename that reside in the input folder will be copied into your output folder.

## Miscellaneous

[HTML Journal](https://journal.miso.town/): You can use [journalize](https://codeberg.org/milofultz/journalize) after your build step to create a valid HTML journal. See [my website's build script](https://codeberg.org/milofultz/milofultz/src/branch/pages/build#L8) for usage.

If you want to clear out all files in your output directory before build, you can use a script like the following (note this deletes **all** files that aren't preceded by a period in your output folder!):

```bash
gemkill="/path/to/gemkill.rb"
input="$1"
output="$2"
site_address="$3"

find "$output" -maxdepth 1 -type f -not -name ".*" -exec rm {} \;
ruby "$gemkill" "$input" "$output" "$site_address"
```

