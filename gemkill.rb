require_relative './site'

if __FILE__ == $0
  if ARGV[0] and ARGV[1]
    input, output, site_url = ARGV
    site = SiteBuilder.new(input, output, site_url)
    site.build()
  else
    puts("Missing arguments. Expecting [input, output]; Received: #{ARGV}")
  end
end

